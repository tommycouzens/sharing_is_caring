#!/bin/bash
ssh -i Tommy.pem -o StrictHostKeyChecking=no ec2-user@3.8.3.36
sudo amazon-linux-extras install -y docker
docker --version
sudo dockerd

# install jq
sudo yum -y install jq

alias sd='sudo docker'

# Run apache
# sd pull httpd
sd run -dit --name apache -p 81:80 httpd
sd inspect apache | jq '.[].NetworkSettings.Networks[].IPAddress'
curl http://localhost:81

# Run nginx
# sd pull nginx
sd run -dit --name nginx -p 82:80 nginx

# Run haproxy
sd run -dit --name haproxy -v \
/home/ec2-user:/usr/local/etc/haproxy:ro -p 80:80 haproxy
