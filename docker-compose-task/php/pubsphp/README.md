# Simple Pubs PHP application

These instructions are based on installing Apache on RHEL based operating systems, e.g.;
* RedHat 7
* Amazon Linux
* Fedora

This application should be installed into your **/var/www/html** directory and you should have installed;
* httpd
* php
* mysqli

You should also ensure that php.conf is in /etc/httpd/conf.d so that PHP will work within Apache.

The php file is executed by specifying the following URL;

http://yourEC2instanceIP/index.php?dbsrv=yourRDSEndPoint&dbuser=yourUsername&dbpass=yourPassword
